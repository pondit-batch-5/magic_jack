import 'dart:core';

import 'package:magic_jack/models/house.dart';

void main() {
  //object
  House royalHouse = House(10,'Bed',1200.00,'Dhaka',false);
  print('Royal room number : ${royalHouse.numberOfRoom}');
  print('Royal room type : ${royalHouse.roomType}');
  print('Royal house address : ${royalHouse.address}');
  royalHouse.address = 'Chittagong';
  print('Royal house address : ${royalHouse.address}');
  // royalHouse.setNumberOfRoom(100);
  // royalHouse.measurement = 1234566.0;
  // print('Royal room number : ${royalHouse.numberOfRoom}');
  // royalHouse.numberOfRoom = 100;
  // print('Royal room measurement : ${royalHouse.measurement}');

  // House urbanHouse = House();
  // urbanHouse.numberOfRoom = 5;
  // urbanHouse.measurement = 1500.0;
  // print('Urban room number : ${urbanHouse.numberOfRoom}');
  // print('Urban room measurement : ${urbanHouse.measurement}');
}
//camelCase [variable, function], PascalCase[class],snake_case,kebab-case


