import 'package:flutter/material.dart';
import 'package:magic_jack/models/card_play.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: Scaffold(
        body: BlackJack(),
      ),
    );
  }
}

class BlackJack extends StatefulWidget {
  const BlackJack({super.key});

  @override
  State<BlackJack> createState() => _BlackJackState();
}

class _BlackJackState extends State<BlackJack> {
  CardPlay cardPlay = CardPlay(2, 2, 0, 0, 0);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.maxFinite,
      width: double.maxFinite,
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/images/background.jpg'),
              fit: BoxFit.cover)),
      child: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Image.asset(
              'assets/images/logo.jpeg',
              height: 130,
              width: 100,
            ),
            Row(
              children: [
                Expanded(
                  child: Image.asset(
                    'assets/images/card${cardPlay.leftCardNumber}.png',
                    height: 130,
                    width: 100,
                  ),
                ),
                Expanded(
                  child: Image.asset(
                    'assets/images/card${cardPlay.rightCardNumber}.png',
                    height: 130,
                    width: 100,
                  ),
                ),
              ],
            ),
            InkWell(
              onTap: () {
                cardPlay.changeCardNumber();
                cardPlay.updateResult();
                setState(() {});
              },
              child: Image.asset(
                'assets/images/dealbutton.png',
                height: 130,
                width: 100,
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Column(
                  children: [
                    Text(
                      'Player',
                      style: TextStyle(
                        fontSize: 20,
                        color: Colors.white,
                      ),
                    ),
                    Text(
                      '${cardPlay.playerScore}',
                      style: TextStyle(
                        fontSize: 22,
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
                Column(
                  children: [
                    Text(
                      'Computer',
                      style: TextStyle(
                        fontSize: 20,
                        color: Colors.white,
                      ),
                    ),
                    Text(
                      '${cardPlay.computerScore}',
                      style: TextStyle(
                        fontSize: 22,
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ],
            ),
            Text(
              'How many time played magic game =  ${cardPlay.playerScore}',
              style: TextStyle(
                fontSize: 20,
                color: Colors.white,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
