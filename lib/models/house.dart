class House {
  //property/field
  int? _numberOfRoom;
  String? _roomType;
  double? _measurement;
  String? _address;
  bool? _isDuplex;

  House(this._numberOfRoom, this._roomType, this._measurement, this._address,
      this._isDuplex);

  // House(int mNumberOfRoom, String mRoomType, double mMeasurement,
  //     String mAddress, bool mIsDuplex) {
  //   _numberOfRoom = mNumberOfRoom;
  //   _roomType = mRoomType;
  //   _measurement = mMeasurement;
  //   _address = mAddress;
  //   _isDuplex = mIsDuplex;
  // }

  set address(String? address) {
    _address = address;
  }

  int? get numberOfRoom => _numberOfRoom;

  String? get roomType => _roomType;

  String? get address => _address;
}
