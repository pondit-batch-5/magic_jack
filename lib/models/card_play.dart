import 'dart:math';

class CardPlay {
  int? _rightCardNumber;
  int? _leftCardNumber;
  int? _computerScore;
  int? _playerScore;
  int? _totalPlayer;

  CardPlay(this._rightCardNumber, this._leftCardNumber, this._computerScore,
      this._playerScore, this._totalPlayer);

  int get rightCardNumber => _rightCardNumber!;

  set rightCardNumber(int value) {
    _rightCardNumber = value;
  }

  int get leftCardNumber => _leftCardNumber!;

  int get totalPlayer => _totalPlayer!;

  set totalPlayer(int value) {
    _totalPlayer = value;
  }

  int get playerScore => _playerScore!;

  set playerScore(int value) {
    _playerScore = value;
  }

  int get computerScore => _computerScore!;

  set computerScore(int value) {
    _computerScore = value;
  }

  set leftCardNumber(int value) {
    _leftCardNumber = value;
  }

  // void leftShuffleCard() {
  //   _leftCardNumber = Random().nextInt(13) + 2;
  // }
  //
  // void righttShuffleCard() {
  //   _rightCardNumber = Random().nextInt(13) + 2;
  // }

  void changeCardNumber() {
    _leftCardNumber = Random().nextInt(13) + 2;
    _rightCardNumber = Random().nextInt(13) + 2;
    _totalPlayer = _totalPlayer! + 1;
  }

  void updateResult() {
    if (_leftCardNumber != _rightCardNumber) {
      if (_leftCardNumber! > _rightCardNumber!) {
        _playerScore = _playerScore! + 1;
      } else {
        _computerScore = _computerScore! + 1;
      }
    }
  }
}
